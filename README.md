# Service Web

TP2 : WPF & Binding MVVM

Cogoni Florian & Mathilde Henry

## Charger la BDD
Pour charger la BDD dans le programme il faut changer le connection string. Pour cela :
- ajouter une nouvelle connexion à une Base (la base se trouve dans ThronesTournamentConsole/BDD/Data.mdf)
- récupérer le connexion string de la BDD
- aller dans ThronesTournamentConsole/DataAccessLayer/DalManager.cs
- changer la ligne 16 avec le nouveau connexion string

### Notes
Pour réussir ce tp, nous avons du corriger des parties de codes du TP précédent :
- toutes les clés étrangères sont des ids
- correction de certaines exceptions (table vide, clé étrangère null ..) dans le DalManager