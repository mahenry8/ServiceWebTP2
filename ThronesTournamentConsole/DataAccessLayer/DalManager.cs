﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using System.Data;
using System.Data.SqlClient;


namespace DataAccessLayer
{
    public class DalManager
    {
        private static DalManager instance;
        private string connexionString = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Téléchargements\\ThronesTournamentConsole\\ThronesTournamentConsole\\ThronesTournamentConsole\\BDD\\Data.mdf;Integrated Security = True; Connect Timeout = 30";


        //"Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=D:\\Téléchargements\\ThronesTournamentConsole (2)\\ThronesTournamentConsole\\ThronesTournamentConsole\\BDD\\Data.mdf;Integrated Security = True; Connect Timeout = 30"
        private DalManager() { }

        public static DalManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DalManager();
                }
                return instance;
            }
        }


        public DataTable SelectByDataAdapter(string request)
        {
            DataTable results = new DataTable();
            SqlConnection sqlConnection;
            using (sqlConnection = new SqlConnection(connexionString))
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter reader;

                cmd.CommandText = request;
                cmd.CommandType = CommandType.Text;
                cmd.Connection = sqlConnection;
                //sqlConnection1.Open();

                reader = new SqlDataAdapter(cmd);
                reader.Fill(results);

                //sqlConnection1.Close();
            }


            return results;
        }

        public House GetHouseByName(String n)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE Name='" + n + "';");
            if (dt.Rows.Count == 0)
                return null;
            DataRow ligne = dt.Rows[0];
            House h = new House(int.Parse(ligne["id"].ToString()), ligne["Name"].ToString(), int.Parse(ligne["NumberOfUnities"].ToString()));
            List<Character> charac = ExistingCharacters();
            h.Housers = charac.FindAll(c => c.IdHouse == h.Id);
            return h;
        }

        public House GetHouseById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE id=" + id + ";");
            if (dt.Rows.Count == 0)
                return null;
            DataRow ligne = dt.Rows[0];
            House h = new House(int.Parse(ligne["id"].ToString()), ligne["Name"].ToString(), int.Parse(ligne["NumberOfUnities"].ToString()));
            List<Character> charac = ExistingCharacters();
            h.Housers = charac.FindAll(c => c.IdHouse == h.Id);
            return h;
        }
        public String GetHouseNameById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE id=" + id + ";");
            if (dt.Rows.Count == 0)
                return null;
            DataRow ligne = dt.Rows[0];
            return ligne["Name"].ToString();
        }
        public List<EntitiesLayer.House> ExistingHouses()
        {
            List<House> ExistingH = new List<House>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses;");

            List<Character> charac = ExistingCharacters();

            foreach (DataRow ligne in dt.Rows)
            {
                House h = new House(int.Parse(ligne["id"].ToString()), ligne["Name"].ToString(), int.Parse(ligne["NumberOfUnities"].ToString()));
                //h.NumberOfUnities = int.Parse(ligne["NumberOfUnities"].ToString());
                h.Housers = charac.FindAll(c => c.IdHouse == h.Id);
                ExistingH.Add(h);
            }

            return ExistingH;
        }

        public List<EntitiesLayer.House> getBigHouses(List<EntitiesLayer.House> ExistingHouses)
        {
            List<House> ExistingH = new List<House>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses WHERE NumberOfUnities >= 200;");

            List<Character> charac = ExistingCharacters();

            foreach (DataRow ligne in dt.Rows)
            {
                House h = new House(int.Parse(ligne["id"].ToString()), ligne["Name"].ToString(), int.Parse(ligne["NumberOfUnities"].ToString()));
                //h.NumberOfUnities = int.Parse(ligne["NumberOfUnities"].ToString());
                h.Housers = charac.FindAll(c => c.IdHouse == h.Id);
                ExistingH.Add(h);
            }

            return ExistingH;
        }

        public War GetWarById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars WHERE Id =" + id.ToString() + ";");
            if (dt.Rows.Count == 0)
                return null;
            DataRow ligne = dt.Rows[0];
            War w = new War(int.Parse(ligne["Id"].ToString()), ligne["Name"].ToString());
            List<Fight> combats = ExistingFights();
            w.combats = combats.FindAll(c => c.idWar == w.id);
            return w;
        }

        public List<EntitiesLayer.War> ExistingWars()
        {
            List<War> ExistingW = new List<War>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars;");
            List<Fight> fights = ExistingFights();
            foreach (DataRow ligne in dt.Rows)
            {
                War w = new War(int.Parse(ligne["Id"].ToString()), ligne["Name"].ToString());
                w.combats = fights.FindAll(f => f.idWar == w.getId());
                ExistingW.Add(w);
            }
            return ExistingW;
        }

        public Fight GetFightById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights WHERE IdFight =" + id.ToString() + ";");
            if (dt.Rows.Count == 0)
                return null;
            DataRow ligne = dt.Rows[0];
            int id1, id2, idw;
            if (ligne["IdHouse1"] == DBNull.Value)
                id1 = -1;
            else
                id1 = int.Parse(ligne["IdHouse1"].ToString());

            if (ligne["IdHouse2"] == DBNull.Value)
                id2 = -1;
            else
                id2 = int.Parse(ligne["IdHouse2"].ToString());
            if (ligne["IdWar"] == DBNull.Value)
                idw = -1;
            else
                idw = int.Parse(ligne["IdWar"].ToString());
            Fight f = new Fight(int.Parse(ligne["IdFight"].ToString()), id1, id2, idw);

            if (ligne["IdWinningHouse"] == DBNull.Value)
                f.IdWinningHouse = -1;
            else
                f.IdWinningHouse = int.Parse(ligne["IdWinningHouse"].ToString());
            return f;
        }

        public List<EntitiesLayer.Fight> ExistingFights()
        {
            List<Fight> ExistingFights = new List<Fight>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");

            foreach (DataRow ligne in dt.Rows)
            {
                int id1, id2, idw;
                if (ligne["IdHouse1"] == DBNull.Value)
                    id1 = -1;
                else
                    id1 = int.Parse(ligne["IdHouse1"].ToString());

                if (ligne["IdHouse2"] == DBNull.Value)
                    id2 = -1;
                else
                    id2 = int.Parse(ligne["IdHouse2"].ToString());
                if (ligne["IdWar"] == DBNull.Value)
                    idw = -1;
                else
                    idw = int.Parse(ligne["IdWar"].ToString());
                Fight f = new Fight(int.Parse(ligne["IdFight"].ToString()),id1, id2, idw);
                if(ligne["IdWinningHouse"] == DBNull.Value)
                    f.IdWinningHouse = -1;
                else
                    f.IdWinningHouse = int.Parse(ligne["IdWinningHouse"].ToString());
                ExistingFights.Add(f);
            }
            return ExistingFights;
        }

        public Territory GetTerritoryById(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories WHERE IdTerritory = " + id.ToString() + ";");
            DataRow ligne = dt.Rows[0];
            Territory t = new Territory(int.Parse(ligne["IdTerritory"].ToString()), (TerritoryType)Enum.Parse(typeof(TerritoryType), ligne["Type"].ToString()), (ligne["IdOwner"].ToString() != "" ?int.Parse(ligne["IdOwner"].ToString()):-1));
            String h = GetHouseNameById(t.IdOwner);
            if (h != null)
                t.HouseName = h;
            else
                t.HouseName = "Aucune";
            return t;
        }

        public List<Territory> ExistingTerritories()
        {
            List<Territory> ExistingT = new List<Territory>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");

            foreach (DataRow ligne in dt.Rows)
            {
                Territory t = new Territory(int.Parse(ligne["IdTerritory"].ToString()), (TerritoryType)Enum.Parse(typeof(TerritoryType), ligne["Type"].ToString()), (ligne["IdOwner"].ToString() != "" ? int.Parse(ligne["IdOwner"].ToString()) : -1));
                String h = GetHouseNameById(t.IdOwner);
                if (h != null)
                    t.HouseName = h;
                else
                    t.HouseName = "Aucune";
                ExistingT.Add(t);
            }
            return ExistingT;
        }

        /*public EntitiesLayer.Character GetCharacterByName(String ln, String fn)
        {
            List<Character> ExistingC = ExistingCharacters();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters WHERE FirstName = '" + fn+ "' and LastName='"+ln+"';");
            DataRow ligne = dt.Rows[0];
            Character c = new Character(int.Parse(ligne["IdCharacter"].ToString()), int.Parse(ligne["Bravoury"].ToString()), int.Parse(ligne["Crazyness"].ToString()), ligne["FirstName"].ToString(), ligne["LastName"].ToString(), int.Parse(ligne["Pv"].ToString()), ligne["HouseName"].ToString(), (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), ligne["CharacterType"].ToString()));
            DataTable R = SelectByDataAdapter("SELECT * FROM dbo.Relationships WHERE IdPerso1 =" + c.id.ToString() + ";");
            foreach (DataRow relation in R.Rows)
            {
                c.AddRelatives((RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), relation["Relationship"].ToString()), ExistingC.Find(x => x.id == int.Parse(relation["IdPerso2"].ToString())));
            }
            return c;
        }*/
        public EntitiesLayer.Character GetCharacterById(int id)
        {
            List<Character> ExistingC = ExistingCharacters();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters WHERE IdCharacter = " + id.ToString() + ";");
            if (dt.Rows.Count == 0)
                return null;
            DataRow ligne = dt.Rows[0];
            Character c = new Character(int.Parse(ligne["IdCharacter"].ToString()), int.Parse(ligne["Bravoury"].ToString()), int.Parse(ligne["Crazyness"].ToString()), ligne["FirstName"].ToString(), ligne["LastName"].ToString(), int.Parse(ligne["Pv"].ToString()), (ligne["IdHouse"].ToString() != "" ? int.Parse(ligne["IdHouse"].ToString()) : -1), (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), ligne["CharacterType"].ToString()));
            String h = GetHouseNameById(c.IdHouse);
            if (h != null)
                c.HouseName = h;
            else
                c.HouseName = "Aucune";
            DataTable R = SelectByDataAdapter("SELECT * FROM dbo.Relationships WHERE IdPerso1 =" + id.ToString() + " or IdPerso2 =" + c.id.ToString() + ";");
            foreach (DataRow relation in R.Rows)
            {
                c.AddRelatives((RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), relation["Relationship"].ToString()), ExistingC.Find(x => x.id == int.Parse(relation["IdPerso2"].ToString()) || x.id == int.Parse(relation["IdPerso1"].ToString())));
            }
            return c;
        }

        public List<EntitiesLayer.Character> ExistingCharacters()
        {
            List<Character> ExistingC = new List<Character>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters;");

            foreach (DataRow ligne in dt.Rows)
            {
                Character c = new Character(int.Parse(ligne["IdCharacter"].ToString()), int.Parse(ligne["Bravoury"].ToString()), int.Parse(ligne["Crazyness"].ToString()), ligne["FirstName"].ToString(), ligne["LastName"].ToString(), int.Parse(ligne["Pv"].ToString()), (ligne["IdHouse"].ToString() != "" ? int.Parse(ligne["IdHouse"].ToString()) : -1), (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), ligne["CharacterType"].ToString()));
                String h = GetHouseNameById(c.IdHouse);
                if (h != null)
                    c.HouseName = h;
                else
                    c.HouseName = "Aucune";
                ExistingC.Add(c);
            }
            foreach (Character c in ExistingC)
            {
                DataTable R = SelectByDataAdapter("SELECT * FROM dbo.Relationships WHERE IdPerso1 =" + c.id.ToString() + " or IdPerso2 =" + c.id.ToString() + ";");
                foreach (DataRow relation in R.Rows)
                {
                    c.AddRelatives((RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), relation["Relationship"].ToString()), ExistingC.Find(x => x.id == int.Parse(relation["IdPerso2"].ToString()) || x.id == int.Parse(relation["IdPerso1"].ToString())));
                }
            }

            return ExistingC;
        }
        public List<EntitiesLayer.Character> CharactersFromHouse(int idh)
        {
            List<Character> ExistingC = new List<Character>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters;");

            foreach (DataRow ligne in dt.Rows)
            {
                if (ligne["IdHouse"].ToString() != "" && int.Parse(ligne["IdHouse"].ToString()) == idh)
                {
                    Character c = new Character(int.Parse(ligne["IdCharacter"].ToString()), int.Parse(ligne["Bravoury"].ToString()), int.Parse(ligne["Crazyness"].ToString()), ligne["FirstName"].ToString(), ligne["LastName"].ToString(), int.Parse(ligne["Pv"].ToString()), int.Parse(ligne["IdHouse"].ToString()), (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), ligne["CharacterType"].ToString()));
                    c.HouseName = GetHouseById(c.IdHouse).Name;
                    ExistingC.Add(c);
                }
            }
            foreach (Character c in ExistingC)
            {
                DataTable R = SelectByDataAdapter("SELECT * FROM dbo.Relationships WHERE IdPerso1 =" + c.id.ToString() + " or IdPerso2 =" + c.id.ToString() + "; ");
                foreach (DataRow relation in R.Rows)
                {
                    c.AddRelatives((RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), relation["Relationship"].ToString()), ExistingC.Find(x => x.id == int.Parse(relation["IdPerso2"].ToString()) || x.id == int.Parse(relation["IdPerso1"].ToString())));
                }
            }

            return ExistingC;
        }


        public List<EntitiesLayer.Territory> TerritoriesFromHouse(int idh)
        {
            List<Territory> ExistingT = new List<Territory>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");

            foreach (DataRow ligne in dt.Rows)
            {
                if (ligne["IdOwner"].ToString()!="" && int.Parse(ligne["IdOwner"].ToString()) == idh)
                {
                    Territory c = new Territory(int.Parse(ligne["IdTerritory"].ToString()), (TerritoryType)Enum.Parse(typeof(TerritoryType), ligne["Type"].ToString()), int.Parse(ligne["IdOwner"].ToString()));
                    c.HouseName = GetHouseById(c.IdOwner).Name;
                    ExistingT.Add(c);
                }
            }
            return ExistingT;
        }

        public List<EntitiesLayer.Fight> FightsFromHouse(int idh)
        {
            List<Fight> ExistingF = new List<Fight>();
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");

            foreach (DataRow ligne in dt.Rows)
            {
                if (int.Parse(ligne["IdHouse1"].ToString()) == idh || int.Parse(ligne["IdHouse2"].ToString()) == idh)
                {
                    int id1, id2, idw;
                    if (ligne["IdHouse1"] == DBNull.Value)
                        id1 = -1;
                    else
                        id1 = int.Parse(ligne["IdHouse1"].ToString());

                    if (ligne["IdHouse2"] == DBNull.Value)
                        id2 = -1;
                    else
                        id2 = int.Parse(ligne["IdHouse2"].ToString());
                    if (ligne["IdWar"] == DBNull.Value)
                        idw = -1;
                    else
                        idw = int.Parse(ligne["IdWar"].ToString());
                    Fight f = new Fight(int.Parse(ligne["IdFight"].ToString()), id1, id2, idw); if (ligne["IdWinningHouse"] == DBNull.Value)
                        f.IdWinningHouse = -1;
                    else
                        f.IdWinningHouse = int.Parse(ligne["IdWinningHouse"].ToString());
                    ExistingF.Add(f);
                }
            }
            return ExistingF;
        }


        private int UpdateByCommandBuilder(String request, DataTable authors)
        {
            int result = 0;
            SqlConnection sqlConnection;
            using (sqlConnection = new SqlConnection(connexionString))
            {
                SqlCommand sqlCommand = new SqlCommand(request, sqlConnection);
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);

                SqlCommandBuilder sqlCommandBuilder = new SqlCommandBuilder(sqlDataAdapter);

                sqlDataAdapter.UpdateCommand = sqlCommandBuilder.GetUpdateCommand();
                sqlDataAdapter.InsertCommand = sqlCommandBuilder.GetInsertCommand();
                sqlDataAdapter.DeleteCommand = sqlCommandBuilder.GetDeleteCommand();

                sqlDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                result = sqlDataAdapter.Update(authors);
            }
            return result;
        }

        public void AddCharacterInBDD(Character charac)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Characters;");
            DataRow myRow = dt.NewRow();
            myRow[1] = charac.Bravoury.ToString();
            myRow[2] = charac.Crazyness.ToString();
            myRow[3] = charac.FirstName;
            myRow[4] = charac.LastName;
            myRow[5] = charac.Pv.ToString();
            if (charac.IdHouse == -1)
                myRow[6] = DBNull.Value;
            else
                myRow[6] = charac.IdHouse;
            myRow[7] = charac.CharacterType.ToString();
            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Characters;", dt);
            if (charac.IdHouse != -1)
            {
                House h = GetHouseById(charac.IdHouse);
                h.AddHousers(charac);
                UpdateHouseInBDD(h);
            }

            dt = SelectByDataAdapter("Select * FROM dbo.Relationships;");
            foreach (var r in charac.Relationships)
            {
                myRow = dt.NewRow();
                myRow[1] = charac.id.ToString();
                myRow[2] = r.Item2.ToString();
                myRow[3] = r.Item1.ToString();
                dt.Rows.Add(myRow);
            }
            UpdateByCommandBuilder("Select * FROM dbo.Relationships;", dt);

        }
        public void AddHouseInBDD(House h)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Houses;");
            DataRow myRow = dt.NewRow();
            myRow[0] = h.Name;
            myRow[1] = h.NumberOfUnities.ToString();
            dt.Rows.Add(myRow);
            UpdateByCommandBuilder("Select * FROM dbo.Houses;", dt);
        }

        public void AddTerritoryInBDD(Territory t)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Territories;");
            DataRow myRow = dt.NewRow();
            myRow[1] = t.Type.ToString();
            if (t.IdOwner == -1)
                myRow[2] = DBNull.Value;
            else
                myRow[2] = t.IdOwner;

            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Territories;", dt);
        }

        public void AddRelationshipInBDD(RelationshipEnum r, int id1, int id2)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Relationships;");
            DataRow myRow = dt.NewRow();
            myRow[1] = id1.ToString();
            myRow[2] = id2.ToString();
            myRow[3] = r.ToString();

            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Relationships;", dt);
        }
        public void AddWarInBDD(War w)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Wars;");
            DataRow myRow = dt.NewRow();
            myRow[1] = w.name;

            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Wars;", dt);
        }
        public void AddFightInBDD(Fight f)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Fights;");
            DataRow myRow = dt.NewRow();
            if (f.IdHouseChallenger1 == -1)
                myRow[1] = DBNull.Value;
            else
                myRow[1] = f.IdHouseChallenger1;
            if (f.IdHouseChallenger2 == -1)
                myRow[2] = DBNull.Value;
            else
                myRow[2] = f.IdHouseChallenger2;
            if (f.IdWinningHouse == -1)
                myRow[3] = DBNull.Value;
            else
                myRow[3] = f.IdWinningHouse;
            if (f.idWar == -1)
                myRow[4] = DBNull.Value;
            else
                myRow[4] = f.idWar;
            dt.Rows.Add(myRow);

            UpdateByCommandBuilder("Select * FROM dbo.Fights;", dt);

            War w = GetWarById(f.idWar);
            if(w != null)
            {
                w.AddFight(f);
                UpdateWarInBDD(w);
            }
        }

        public void DeleteCharacterInBDD(int id)
        {
            Character c = GetCharacterById(id);
            if (c.IdHouse != -1)
            {
                House h = GetHouseById(c.IdHouse);
                h.RemoveHousers(c);
                UpdateHouseInBDD(h);
            }

            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Characters;");

            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["IdCharacter"].ToString()) == id)
                    row.Delete();
            }
            UpdateByCommandBuilder("Select * FROM dbo.Characters;", dt);

            dt = SelectByDataAdapter("Select * FROM dbo.Relationships;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["IdPerso1"].ToString()) == id || int.Parse(row["IdPerso2"].ToString()) == id)
                    row.Delete();
            }
            UpdateByCommandBuilder("Select * FROM dbo.Relationships;", dt);
        }

        public void DeleteHouseInBDD(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["id"].ToString()) == id)
                {
                    foreach (Character c in CharactersFromHouse(id))
                    {
                        c.IdHouse = -1;
                        UpdateCharacterInBDD(c);
                    }
                    foreach (Territory t in TerritoriesFromHouse(id))
                    {
                        t.IdOwner = -1;
                        UpdateTerritoryInBDD(t);
                    }
                    foreach (Fight f in FightsFromHouse(id))
                    {
                        DeleteFightInBDD(f.id);
                    }
                    row.Delete();
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Houses;", dt);
        }

        public void DeleteRelationshipInBDD(int id1, int id2)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Relationships;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if ((int.Parse(row["IdPerso1"].ToString()) == id1 && int.Parse(row["IdPerso2"].ToString()) == id2) || (int.Parse(row["IdPerso2"].ToString()) == id1 && int.Parse(row["IdPerso2"].ToString()) == id2))
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Relationships;", dt);
        }

        public void DeleteTerritoryInBDD(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["IdTerritory"].ToString()) == id)
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Territories;", dt);
        }

        public void DeleteFightInBDD(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["IdFight"].ToString()) == id)
                {
                    if (row["IdWar"] != DBNull.Value)
                    {
                        War w = GetWarById(int.Parse(row["IdWar"].ToString()));
                        if(w!= null)
                        {
                            w.RemoveFight(GetFightById(id));
                            UpdateWarInBDD(w);
                        }
                        
                    }

                    row.Delete();
                }

            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Fights;", dt);

        }
        public void DeleteWarInBDD(int id)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars;");
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
            {
                DataRow row = dt.Rows[i];
                if (int.Parse(row["Id"].ToString()) == id)
                    row.Delete();
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Wars;", dt);
        }
        public void UpdateCharacterInBDD(Character charac)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Characters;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow["IdCharacter"].ToString()) == charac.id)
                {
                    if (charac.IdHouse!= -1 && myRow[6]!=DBNull.Value && int.Parse(myRow[6].ToString()) != charac.IdHouse)
                    {
                        House h = GetHouseById(int.Parse(myRow[6].ToString()));
                        h.RemoveHousers(charac);
                        UpdateHouseInBDD(h);

                        h = GetHouseById(charac.IdHouse);
                        if(h != null)
                        {
                            h.AddHousers(charac);
                            UpdateHouseInBDD(h);
                        }
                        
                    }
                    myRow[1] = charac.Bravoury.ToString();
                    myRow[2] = charac.Crazyness.ToString();
                    myRow[3] = charac.FirstName;
                    myRow[4] = charac.LastName;
                    myRow[5] = charac.Pv.ToString();
                    if (charac.IdHouse != -1)
                        myRow[6] = charac.IdHouse.ToString();
                    else
                        myRow[6] = DBNull.Value;
                    myRow[7] = charac.CharacterType.ToString();
                }
            }

            UpdateByCommandBuilder("SELECT * FROM dbo.Characters;", dt);


        }
        public void UpdateHouseInBDD(House h)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Houses;");
            foreach (DataRow myRow in dt.Rows)
            {

                if (int.Parse(myRow["id"].ToString()) == h.Id)
                {
                    myRow[1] = h.NumberOfUnities.ToString();
                    myRow[0] = h.Name;
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Houses;", dt);


        }

        public void UpdateTerritoryInBDD(Territory t)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Territories;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow[0].ToString()) == t.id)
                {
                    myRow[1] = t.Type.ToString();
                    if (t.IdOwner != -1)
                        myRow[2] = t.IdOwner.ToString();
                    else
                        myRow[2] = DBNull.Value;
                }

            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Territories;", dt);
        }

        public void UpdateFightInBDD(Fight f)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Fights;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow[0].ToString()) == f.id)
                {
                    if (f.IdHouseChallenger1 == -1)
                        myRow[1] = DBNull.Value;
                    else
                        myRow[1] = f.IdHouseChallenger1;
                    if (f.IdHouseChallenger2 == -1)
                        myRow[2] = DBNull.Value;
                    else
                        myRow[2] = f.IdHouseChallenger2;
                    if (f.IdWinningHouse == -1)
                        myRow[3] = DBNull.Value;
                    else
                        myRow[3] = f.IdWinningHouse;
                    if (f.idWar == -1)
                        myRow[4] = DBNull.Value;
                    else
                        myRow[4] = f.idWar;
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Fights;", dt);
        }

        public void UpdateWarInBDD(War w)
        {
            DataTable dt = SelectByDataAdapter("SELECT * FROM dbo.Wars;");
            foreach (DataRow myRow in dt.Rows)
            {
                if (int.Parse(myRow[0].ToString()) == w.getId())
                {
                    myRow[1] = w.name;
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Wars;", dt);
        }

        public void UpdateRelationshipInBDD(RelationshipEnum r, int id1, int id2)
        {
            DataTable dt = SelectByDataAdapter("Select * FROM dbo.Relationships;");
            foreach (DataRow row in dt.Rows)
            {
                if ((int.Parse(row["IdPerso1"].ToString()) == id1 && int.Parse(row["IdPerso2"].ToString()) == id2) || (int.Parse(row["IdPerso2"].ToString()) == id1 && int.Parse(row["IdPerso2"].ToString()) == id2))
                {
                    row[1] = id1.ToString();
                    row[2] = id2.ToString();
                    row[3] = r.ToString();
                }
            }
            UpdateByCommandBuilder("SELECT * FROM dbo.Relationships;", dt);
        }

    }
}
