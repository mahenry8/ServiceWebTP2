﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GameOfThronesTournamentWPF.ViewModel;

namespace GameOfThronesTournamentWPF.View
{
    public partial class ExporterView : System.Windows.Controls.UserControl
    {
        public ExporterView()
        {
            InitializeComponent();
        }
        private void btnChemin_Click(object sender, System.EventArgs e)
        {
            Console.WriteLine("coucou");
            ExporterViewModel c = gridEdition.DataContext as ExporterViewModel;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "(*.xml)|*.xml";
            saveFileDialog1.Title = "Exporter les listes";
            saveFileDialog1.ShowDialog();
            c.Save(saveFileDialog1.FileName);

            
        }
    }
}
