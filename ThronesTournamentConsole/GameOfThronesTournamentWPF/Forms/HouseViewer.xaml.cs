﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{
    /// <summary>
    /// Logique d'interaction pour HouseViewer.xaml
    /// </summary>
    public partial class HouseViewer : Window
    {
        private API.Models.HouseDTO _source;

        public HouseViewer() : this(null) { }

        public HouseViewer(API.Models.HouseDTO source)
        {
            InitializeComponent();

            if (source == null)
            {
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                _source = hc.GetAllHouses()[0];
            }
            else _source = source;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.HouseViewModel avm = new GameOfThronesTournamentWPF.ViewModel.HouseViewModel(_source);
            ucHouse.DataContext = avm;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
