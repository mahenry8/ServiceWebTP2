﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{
    /// <summary>
    /// Logique d'interaction pour HousesViewer.xaml
    /// </summary>
    public partial class HousesViewer : Window
    {
        public HousesViewer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            API.Controllers.HouseController hc = new API.Controllers.HouseController();
            IList<API.Models.HouseDTO> houses = hc.GetAllHouses();

            ViewModel.HousesViewModel avm = new GameOfThronesTournamentWPF.ViewModel.HousesViewModel(houses);
            avm.CloseAsk += CloseAsk;
            ucHouses.DataContext = avm;
        }

        private void CloseAsk(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.HousesViewModel avm = null;

            avm = (ViewModel.HousesViewModel)ucHouses.DataContext;
            if (avm != null) avm.CloseAsk -= CloseAsk;
        }

    }
}
