﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{

    public partial class CharactersViewer : Window
    {
        public CharactersViewer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            API.Controllers.CharacterController ch = new API.Controllers.CharacterController();
            IList<API.Models.CharacterDTO> characters = ch.GetAllCharacters();

            ViewModel.CharactersViewModel avm = new GameOfThronesTournamentWPF.ViewModel.CharactersViewModel(characters);
            avm.CloseAsk += CloseAsk;
            ucCharacters.DataContext = avm;
        }

        private void CloseAsk(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.CharactersViewModel avm = null;

            avm = (ViewModel.CharactersViewModel)ucCharacters.DataContext;
            if (avm != null) avm.CloseAsk -= CloseAsk;
        }
    }
}
