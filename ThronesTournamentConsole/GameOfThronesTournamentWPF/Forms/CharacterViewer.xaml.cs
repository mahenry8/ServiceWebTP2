﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{
    /// <summary>
    /// Logique d'interaction pour CharacterViewer.xaml
    /// </summary>
    public partial class CharacterViewer : Window
    {
        private API.Models.CharacterDTO _source;

        public CharacterViewer() : this(null) { }

        public CharacterViewer(API.Models.CharacterDTO source)
        {
            InitializeComponent();

            if (source == null)
            {
                API.Controllers.CharacterController ch = new API.Controllers.CharacterController();
                _source = ch.GetAllCharacters()[0];
            }
            else _source = source;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.CharacterViewModel avm = new GameOfThronesTournamentWPF.ViewModel.CharacterViewModel(_source);
            ucCharacter.DataContext = avm;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
