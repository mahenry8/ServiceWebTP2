﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{
   
    public partial class FightViewer : Window
    {
        private API.Models.FightDTO _source;

        public FightViewer() : this(null) { }

        public FightViewer(API.Models.FightDTO source)
        {
            InitializeComponent();

            if (source == null)
            {
                API.Controllers.FightController fc = new API.Controllers.FightController();
                _source = fc.GetAllFights()[0];
            }
            else _source = source;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.FightViewModel avm = new GameOfThronesTournamentWPF.ViewModel.FightViewModel(_source);
            ucFight.DataContext = avm;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
