﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{

    public partial class TerritoriesViewer : Window
    {
        public TerritoriesViewer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            API.Controllers.TerritoryController tc = new API.Controllers.TerritoryController();
            IList<API.Models.TerritoryDTO> territory = tc.GetAllTerritories();

            ViewModel.TerritoriesViewModel avm = new GameOfThronesTournamentWPF.ViewModel.TerritoriesViewModel(territory);
            avm.CloseAsk += CloseAsk;
            ucTerritories.DataContext = avm;
        }

        private void CloseAsk(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.TerritoriesViewModel avm = null;

            avm = (ViewModel.TerritoriesViewModel)ucTerritories.DataContext;
            if (avm != null) avm.CloseAsk -= CloseAsk;
        }
    }
}
