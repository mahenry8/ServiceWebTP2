﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{

    public partial class FightsViewer : Window
    {
        public FightsViewer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            API.Controllers.FightController fc = new API.Controllers.FightController();
            IList<API.Models.FightDTO> fights = fc.GetAllFights();

            ViewModel.FightsViewModel avm = new GameOfThronesTournamentWPF.ViewModel.FightsViewModel(fights);
            avm.CloseAsk += CloseAsk;
            ucFights.DataContext = avm;
        }

        private void CloseAsk(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.FightsViewModel avm = null;

            avm = (ViewModel.FightsViewModel)ucFights.DataContext;
            if (avm != null) avm.CloseAsk -= CloseAsk;
        }
    }
}
