﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{
    /// <summary>
    /// Logique d'interaction pour ExporterViewer.xaml
    /// </summary>
    public partial class ExporterViewer : Window
    {
        public ExporterViewer()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.ExporterViewModel avm = new GameOfThronesTournamentWPF.ViewModel.ExporterViewModel();
            avm.CloseAsk += CloseAsk;
            ucExporter.DataContext = avm;
        }

        private void CloseAsk(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            ViewModel.ExporterViewModel avm = null;

            avm = (ViewModel.ExporterViewModel)ucExporter.DataContext;
            if (avm != null) avm.CloseAsk -= CloseAsk;
        }
    }
}
