﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameOfThronesTournamentWPF.Forms
{
    public partial class TerritoryViewer : Window
    {
        private API.Models.TerritoryDTO _source;

        public TerritoryViewer() : this(null) { }

        public TerritoryViewer(API.Models.TerritoryDTO source)
        {
            InitializeComponent();

            if (source == null)
            {
                API.Controllers.TerritoryController tc = new API.Controllers.TerritoryController();
                _source = tc.GetAllTerritories()[0];
            }
            else _source = source;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel.TerritoryViewModel avm = new GameOfThronesTournamentWPF.ViewModel.TerritoryViewModel(_source);
            ucTerritory.DataContext = avm;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
