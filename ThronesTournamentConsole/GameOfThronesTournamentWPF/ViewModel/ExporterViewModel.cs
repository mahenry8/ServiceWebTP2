﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace GameOfThronesTournamentWPF.ViewModel
{
    public class Type
    {
        public bool Houses;
        public bool Characters;
        public bool Fights;
        public bool Territories;


    }
    public class ExporterViewModel : ViewModelBase
    {
        public event EventHandler<EventArgs> CloseAsk;

        protected void OnCloseAsk(EventArgs e)
        {
            this.CloseAsk(this, e);
        }
        private bool CanClose()
        {
            return true;
        }

        private void Close()
        {
            OnCloseAsk(new EventArgs());
        }
        private RelayCommand _closeCommand;
        public System.Windows.Input.ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        () => this.Close(),
                        () => this.CanClose()
                        );
                }
                return _closeCommand;
            }
        }

        private Type _type;
        public ExporterViewModel()
        {
            _type = new Type();
        }

        public ExporterViewModel(String _chemin)
        {
                if(_chemin.Length != 0)
                {
                    StreamWriter stream = new StreamWriter(@_chemin);
                    XmlSerializer serializer = new XmlSerializer(typeof(API.Models.CharacterDTO));
                    API.Controllers.CharacterController hc = new API.Controllers.CharacterController();
                    serializer.Serialize(stream, hc.GetAllCharacters());
                    stream.Close();
                }
            
           
        }

        #region "Propriétés accessibles, mappables par la View"

        public Type Type
        {
            get { return _type; }
            set
            {
                if (value == _type) return;
                _type = value;
            }
        }

        public bool Houses
        {
            get { return _type.Houses; }
            set
            {
                _type.Houses = value;
            }
        }
        public bool Fights
        {
            get { return _type.Fights; }
            set
            {
                _type.Fights = value;
                
            }
        }
        public bool Characters
        {
            get { return _type.Characters; }
            set
            {
                _type.Characters = value;
            }
        }
        public bool Territories
        {
            get { return _type.Territories; }
            set
            {
                _type.Territories = value;
            }
        }

        public void Save(String _chemin)
        {
            StreamWriter stream = new StreamWriter(@_chemin);

            XmlSerializer serializer;
            if (_type.Houses)
            {
                serializer = new XmlSerializer(typeof(List<API.Models.HouseDTO>));
                API.Controllers.HouseController c = new API.Controllers.HouseController();
                serializer.Serialize(stream, c.GetAllHouses());
            }
            if (_type.Characters)
            {
                serializer = new XmlSerializer(typeof(List<API.Models.CharacterDTO>));
                API.Controllers.CharacterController hc = new API.Controllers.CharacterController();
                serializer.Serialize(stream, hc.GetAllCharacters());
            }
            if (_type.Territories)
            {
                serializer = new XmlSerializer(typeof(List<API.Models.TerritoryDTO>));
                API.Controllers.TerritoryController cc = new API.Controllers.TerritoryController();
                serializer.Serialize(stream, cc.GetAllTerritories());
            }
            if (_type.Fights)
            {
                serializer = new XmlSerializer(typeof(List<API.Models.FightDTO>));
                API.Controllers.FightController fc = new API.Controllers.FightController();
                serializer.Serialize(stream, fc.GetAllFights());
            }

            stream.Close();
        }


        #endregion

    }
}


