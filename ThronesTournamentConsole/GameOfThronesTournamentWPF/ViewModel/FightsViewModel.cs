﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    class FightsViewModel : ViewModelBase
    {
        public event EventHandler<EventArgs> CloseAsk;

        protected void OnCloseAsk(EventArgs e)
        {
            this.CloseAsk(this, e);
        }

        private ObservableCollection<FightViewModel> _fights;

        public ObservableCollection<FightViewModel> Fights
        {
            get { return _fights; }
            private set
            {
                _fights = value;
                OnPropertyChanged("Fights");
            }
        }
        private CtrlHouseViewModel _selectedHouse1;
        public CtrlHouseViewModel SelectedHouse1
        {
            get { return _selectedHouse1; }
            set
            {
                _selectedHouse1 = value;
                OnPropertyChanged("SelectedHouse1");
            }
        }
        private CtrlHouseViewModel _selectedHouse2;
        public CtrlHouseViewModel SelectedHouse2
        {
            get { return _selectedHouse2; }
            set
            {
                _selectedHouse2 = value;
                OnPropertyChanged("SelectedHouse2");
            }
        }
        private CtrlHouseViewModel _selectedHouseW;
        public CtrlHouseViewModel SelectedHouseW
        {
            get { return _selectedHouseW; }
            set
            {
                _selectedHouseW = value;
                OnPropertyChanged("SelectedHouseW");
            }
        }

        private FightViewModel _selectedItem;
        public FightViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    API.Controllers.HouseController hc = new API.Controllers.HouseController();
                    API.Models.HouseDTO h = hc.GetHouse(value.IdHouseChallenger1);
                    if (h == null)
                        _selectedHouse1 = new CtrlHouseViewModel(new API.Models.HouseDTO(-1, "Aucune", 0));
                    else
                        _selectedHouse1 = new CtrlHouseViewModel(h);
                    OnPropertyChanged("SelectedHouse1");

                    h = hc.GetHouse(value.IdHouseChallenger2);
                    if (h == null)
                        _selectedHouse2 = new CtrlHouseViewModel(new API.Models.HouseDTO(-1, "Aucune", 0));
                    else
                        _selectedHouse2 = new CtrlHouseViewModel(h);
                    OnPropertyChanged("SelectedHouse2");

                    h = hc.GetHouse(value.IdWinningHouse);
                    if (h == null)
                        _selectedHouseW = new CtrlHouseViewModel(new API.Models.HouseDTO(-1, "Aucune", 0));
                    else
                        _selectedHouseW = new CtrlHouseViewModel(h);
                    OnPropertyChanged("SelectedHouseW");

                    OnPropertyChanged("SelectedItem");
                }
            }
        }

        public FightsViewModel(IList<API.Models.FightDTO> FightsModel)
        {
            _fights = new ObservableCollection<FightViewModel>();
            foreach(API.Models.FightDTO f in FightsModel)
            {
                _fights.Add(new FightViewModel(f));
            }
        }

        #region "Commandes du formulaire"

        // Commande Add
        private RelayCommand _addCommand;
        public System.Windows.Input.ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(
                        () => this.Add(),
                        () => this.CanAdd()
                        );
                }
                return _addCommand;
            }
        }

        private bool CanAdd()
        {
            return true;
        }

        private void Add()
        {
            API.Models.FightDTO f = new API.Models.FightDTO(-1,-1, -1, -1, -1);
            this.SelectedItem = new FightViewModel(f);
            this.SelectedItem.addInBDD();
            Fights.Add(this.SelectedItem);
            API.Controllers.FightController ch = new API.Controllers.FightController();
            SelectedItem.Id = ch.GetAllFights().Last().id;
        }

        // Commande Remove
        private RelayCommand _removeCommand;
        public System.Windows.Input.ICommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(
                        () => this.Remove(),
                        () => this.CanRemove()
                        );
                }
                return _removeCommand;
            }
        }

        private bool CanRemove()
        {
            return (this.SelectedItem != null);
        }

        private void Remove()
        {
            if (this.SelectedItem != null)
            {
                API.Controllers.FightController fc = new API.Controllers.FightController();
                fc.DeleteFight(this.SelectedItem.Id);
                Fights.Remove(this.SelectedItem);
            }
        }

        // Commande Close
        private RelayCommand _closeCommand;
        public System.Windows.Input.ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        () => this.Close(),
                        () => this.CanClose()
                        );
                }
                return _closeCommand;
            }
        }

        private bool CanClose()
        {
            return true;
        }

        private void Close()
        {
            OnCloseAsk(new EventArgs());
        }

        #endregion
    }

}
