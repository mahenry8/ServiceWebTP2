﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    public class TerritoryViewModel : ViewModelBase
    {
        private API.Models.TerritoryDTO _territory;

        public API.Models.TerritoryDTO Territory
        {
            get { return _territory; }
            private set { _territory = value; }
        }

        public TerritoryViewModel(API.Models.TerritoryDTO t)
        {
            _territory = t;

        }
        public void addInBDD()
        {
            API.Controllers.TerritoryController tc = new API.Controllers.TerritoryController();
            if (_territory.id == -1)
                tc.AddTerritory(new API.Models.TerritoryDTO(_territory.Type, _territory.IdOwner, _territory.HouseName));
            else
                tc.PutTerritory(new API.Models.TerritoryDTO(_territory.id, _territory.Type, _territory.IdOwner, _territory.HouseName));
        }

        #region "Propriétés accessibles, mappables par la View"
        public int Id
        {
            get { return _territory.id; }
            set
            {
                _territory.id = value;
            }
        }
        public String HouseName
        {
            get { return _territory.HouseName; }
            set
            {
                if (value == _territory.HouseName) return;
                if (base.OnCheckHouseName(value) == 0) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                int idh = hc.GetHouse(value).id;
                _territory.HouseName = value;
                _territory.IdOwner = idh;
                base.OnPropertyChanged("IdOwner");
                base.OnPropertyChanged("HouseName");
                addInBDD();
            }
        }
        public EntitiesLayer.TerritoryType Type
        {
            get { return _territory.Type; }
            set
            {
                if (value == _territory.Type) return;
                _territory.Type = value;
                base.OnPropertyChanged("Type");
                addInBDD();
            }
        }
        public int IdOwner
        {
            get { return _territory.IdOwner; }
            set
            {
                if (value == _territory.IdOwner) return;
                _territory.IdOwner = value;
                base.OnPropertyChanged("IdOwner");
                addInBDD();
            }
        }
        #endregion

    }
}


