﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    public class CtrlHouseViewModel : ViewModelBase
    {
        private API.Models.HouseDTO _house;

        public API.Models.HouseDTO House
        {
            get { return _house; }
            private set { _house = value; }
        }

        public CtrlHouseViewModel(API.Models.HouseDTO h)
        {
            _house = h;

        }

        #region "Propriétés accessibles, mappables par la View"

        public string Name
        {
            get { return _house.Name; }
            set
            {
                if (value == _house.Name) return;
                if(base.OnHouseNameChanged(value)==0) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                if (_house.Name == "<New>")
                    hc.AddHouse(new API.Models.HouseDTO(value, _house.NumberOfUnities));
                else
                {
                    int id = hc.GetHouse(_house.Name).id;
                    hc.PutHouse(new API.Models.HouseDTO(id, value, _house.NumberOfUnities));
                }
                    
                _house.Name = value;
                base.OnPropertyChanged("Name");
            }
        }

        public int Id
        {
            get { return _house.id; }
            set
            {
                _house.id = value;
            }
        }

        public int NumberOfUnities
        {
            get { return _house.NumberOfUnities; }
            set
            {
                if (value == _house.NumberOfUnities) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                if (_house.Name != "<New>")
                {
                    int id = hc.GetHouse(_house.Name).id;
                    hc.PutHouse(new API.Models.HouseDTO(id, _house.Name, value));
                }
                else
                    hc.AddHouse(new API.Models.HouseDTO(_house.Name, value));
                _house.NumberOfUnities = value;
                base.OnPropertyChanged("NumberOfUnities");
            }
        }
        #endregion

    }
}


