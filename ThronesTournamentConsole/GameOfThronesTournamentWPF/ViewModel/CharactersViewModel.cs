﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    class CharactersViewModel : ViewModelBase
    {
        public event EventHandler<EventArgs> CloseAsk;
        
        protected void OnCloseAsk(EventArgs e)
        {
            this.CloseAsk(this, e);
        }

        private ObservableCollection<CharacterViewModel> _characters;

        public ObservableCollection<CharacterViewModel> Characters
        {
            get { return _characters; }
            private set
            {
                _characters = value;
                OnPropertyChanged("Characters");
             }
        }
        private CtrlHouseViewModel _selectedHouse;
        public CtrlHouseViewModel SelectedHouse
        {
            get { return _selectedHouse; }
            set
            {
                _selectedHouse = value;
                OnPropertyChanged("SelectedHouse");
            }
        }

        private CharacterViewModel _selectedItem;
        public CharacterViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    API.Controllers.HouseController hc = new API.Controllers.HouseController();
                    API.Models.HouseDTO h = hc.GetHouse(value.IdHouse);
                    if (h == null)
                        _selectedHouse = new CtrlHouseViewModel(new API.Models.HouseDTO(-1, "Aucune", 0));
                    else
                        _selectedHouse = new CtrlHouseViewModel(h);
                    OnPropertyChanged("SelectedHouse");
                    OnPropertyChanged("SelectedItem");
                }
            }
        }

        

        public CharactersViewModel(IList<API.Models.CharacterDTO> CharactersModel)
        {
            _characters = new ObservableCollection<CharacterViewModel>();
            foreach (API.Models.CharacterDTO c in CharactersModel)
            {
                _characters.Add(new CharacterViewModel(c));
            }
        }

        #region "Commandes du formulaire"

        // Commande Add
        private RelayCommand _addCommand;
        public System.Windows.Input.ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(
                        () => this.Add(),
                        () => this.CanAdd()
                        );
                }
                return _addCommand;
            }
        }

        private bool CanAdd()
        {
            return true;
        }

        private void Add()
        {
            API.Models.CharacterDTO c = new API.Models.CharacterDTO(-1,0,0,"<New>", "<New>",0,-1,"aucune",EntitiesLayer.CharacterTypeEnum.LEADER);
            this.SelectedItem = new CharacterViewModel(c);
            this.SelectedItem.addInBDD();
            Characters.Add(this.SelectedItem);
            API.Controllers.CharacterController ch = new API.Controllers.CharacterController();
            SelectedItem.Id = ch.GetAllCharacters().Last().id;
            
        }

        // Commande Remove
        private RelayCommand _removeCommand;
        public System.Windows.Input.ICommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(
                        () => this.Remove(),
                        () => this.CanRemove()
                        );
                }
                return _removeCommand;
            }
        }

        private bool CanRemove()
        {
            return (this.SelectedItem != null);
        }

        private void Remove()
        {
            if (this.SelectedItem != null)
            {
                API.Controllers.CharacterController hc = new API.Controllers.CharacterController();
                hc.DeleteCharacter(this.SelectedItem.Id);
                Characters.Remove(this.SelectedItem);
            }
        }

        // Commande Close
        private RelayCommand _closeCommand;
        public System.Windows.Input.ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        () => this.Close(),
                        () => this.CanClose()
                        );
                }
                return _closeCommand;
            }
        }

        private bool CanClose()
        {
            return true;
        }

        private void Close()
        {
            OnCloseAsk(new EventArgs());
        }

        #endregion
    }

}
