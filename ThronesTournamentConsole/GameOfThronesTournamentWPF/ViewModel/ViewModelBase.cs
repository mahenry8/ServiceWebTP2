﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.ComponentModel;
    using System.Diagnostics;

    public abstract class ViewModelBase : INotifyPropertyChanged
    {
           
        public event PropertyChangedEventHandler PropertyChanged;

            
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                Debug.Fail("Invalid property name: " + propertyName);
            }
        }

           
        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);

            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        
     
        public int VerifyHouseName(string Name)
        {
            API.Controllers.HouseController hc = new API.Controllers.HouseController();
            foreach(API.Models.HouseDTO h in hc.GetAllHouses())
            {
                if(h.Name == Name)
                {
                    Debug.Fail("Le nom est déjà utilisé : " + Name);
                    return 0;
                }
                   
            }
            return 1;
        }

        protected virtual int OnHouseNameChanged(string propertyName)
        {
            return this.VerifyHouseName(propertyName);
        }

        public int ExistHouseName(string Name)
        {
            API.Controllers.HouseController hc = new API.Controllers.HouseController();
            foreach (API.Models.HouseDTO h in hc.GetAllHouses())
            {
                if (h.Name == Name)
                {
                    return 1;
                }
            }
            Debug.Fail("Le nom n'existe pas : " + Name);
            return 0;
        }
        protected virtual int OnCheckHouseName(string propertyName)
        {
            return this.ExistHouseName(propertyName);
        }
    }

}
