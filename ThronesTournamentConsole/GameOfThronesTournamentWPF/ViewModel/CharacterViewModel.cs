﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    public class CharacterViewModel : ViewModelBase
    {
        private API.Models.CharacterDTO _character;

        public API.Models.CharacterDTO Character
        {
            get { return _character; }
            private set { _character = value; }
        }

        public CharacterViewModel(API.Models.CharacterDTO c)
        {
            _character = c;
        }

        public void addInBDD()
        {
            API.Controllers.CharacterController hc = new API.Controllers.CharacterController();
            if(_character.id == -1)
                hc.AddCharacter(new API.Models.CharacterDTO(_character.Bravoury, _character.Crazyness, _character.FirstName, _character.LastName, _character.Pv, _character.IdHouse, _character.HouseName, _character.CharacterType));
            else
                hc.PutCharacter(new API.Models.CharacterDTO(_character.id, _character.Bravoury, _character.Crazyness, _character.FirstName, _character.LastName, _character.Pv, _character.IdHouse, _character.HouseName, _character.CharacterType));
        }
        #region "Propriétés accessibles, mappables par la View"
        public int Id
        {
            get { return _character.id; }
            set
            {
                _character.id = value;
            }
        }
        public string FirstName
        {
            get { return _character.FirstName; }
            set
            {
                if (value == _character.FirstName) return;
                _character.FirstName = value;
                base.OnPropertyChanged("FirstName");
                addInBDD();
            }
        }
        public string LastName
        {
            get { return _character.LastName; }
            set
            {
                if (value == _character.LastName) return;
                _character.LastName = value;
                base.OnPropertyChanged("LastName");
                addInBDD();
            }
        }
        
        public int Bravoury
        {
            get { return _character.Bravoury; }
            set
            {
                if (value == _character.Bravoury) return;
                _character.Bravoury = value;
                base.OnPropertyChanged("Bravoury");
                addInBDD();
            }
        }

        public int Crazyness
        {
            get { return _character.Crazyness; }
            set
            {
                if (value == _character.Crazyness) return;
                _character.Crazyness = value;
                base.OnPropertyChanged("Crazyness");
                addInBDD();
            }
        }

        public int Pv
        {
            get { return _character.Pv; }
            set
            {
                if (value == _character.Pv) return;
                API.Controllers.CharacterController hc = new API.Controllers.CharacterController();
                _character.Pv = value;
                base.OnPropertyChanged("Pv");
                addInBDD();
            }
        }
        public int IdHouse
        {
            get { return _character.IdHouse; }
            set
            {
                _character.IdHouse = value;
                base.OnPropertyChanged("IdHouse");
                
            }
        }
        public String HouseName
        {
            get { return _character.HouseName; }
            set
            {
                if (value == _character.HouseName) return;
                if (base.OnCheckHouseName(value) == 0) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                int idh = hc.GetHouse(value).id;
                
                _character.HouseName = value;
                _character.IdHouse = idh;
                base.OnPropertyChanged("IdHouse");
                base.OnPropertyChanged("HouseName");
                addInBDD();
            }
        }
        public EntitiesLayer.CharacterTypeEnum CharacterType
        {
            get { return _character.CharacterType; }
            set
            {
                if (value == _character.CharacterType) return;
                _character.CharacterType = value;
                base.OnPropertyChanged("CharacterType");
                addInBDD();
            }
        }

        #endregion

    }
}


