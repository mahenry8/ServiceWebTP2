﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    public class FightViewModel : ViewModelBase
    {
        private API.Models.FightDTO _fight;

        public API.Models.FightDTO Character
        {
            get { return _fight; }
            private set { _fight = value; }
        }

        public FightViewModel(API.Models.FightDTO f)
        {
            _fight = f;
            if(_fight.IdHouseChallenger1 == -1)
                _fight.HouseName1 = "Aucune";
            else
            {
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                _fight.HouseName1 = hc.GetHouse(_fight.IdHouseChallenger1).Name;
            }
            if (_fight.IdHouseChallenger2== -1)
                _fight.HouseName2 = "Aucune";
            else
            {
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                _fight.HouseName2 = hc.GetHouse(_fight.IdHouseChallenger2).Name;
            }
            if (_fight.IdWinningHouse == -1)
                _fight.HouseNameW = "Aucune";
            else
            {
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                _fight.HouseNameW = hc.GetHouse(_fight.IdWinningHouse).Name;
            }

        }
        public void addInBDD()
        {
            API.Controllers.FightController fc = new API.Controllers.FightController();
            if (_fight.id == -1)
                fc.AddFight(new API.Models.FightDTO(_fight.IdHouseChallenger1, _fight.IdHouseChallenger2, _fight.IdWinningHouse, _fight.IdWar));
            else
                fc.PutFight(new API.Models.FightDTO(_fight.id, _fight.IdHouseChallenger1, _fight.IdHouseChallenger2, _fight.IdWinningHouse, _fight.IdWar));
        }

        #region "Propriétés accessibles, mappables par la View"
        public int Id
        {
            get { return _fight.id; }
            set
            {
                _fight.id = value;
            }
        }
        public int IdHouseChallenger1
        {
            get { return _fight.IdHouseChallenger1; }
            set
            {
                if (value == _fight.IdHouseChallenger1) return;
                _fight.IdHouseChallenger1 = value;
                base.OnPropertyChanged("IdHouseChallenger1");
                addInBDD();
            }
        }
        public String HouseName1
        {
            get { return _fight.HouseName1; }
            set
            {
                if (value == _fight.HouseName1) return;
                if (base.ExistHouseName(value) == 0) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                int idh = hc.GetHouse(value).id;
                _fight.HouseName1 = value;
                _fight.IdHouseChallenger1 = idh;
                base.OnPropertyChanged("HouseName1");
                addInBDD();
            }
        }
        public int IdHouseChallenger2
        {
            get { return _fight.IdHouseChallenger2; }
            set
            {
                if (value == _fight.IdHouseChallenger2) return;
                _fight.IdHouseChallenger2 = value;
                base.OnPropertyChanged("IdHouseChallenger2");
                addInBDD();
            }
        }
        public String HouseName2
        {
            get { return _fight.HouseName2; }
            set
            {
                if (value == _fight.HouseName2) return;
                if (base.ExistHouseName(value) == 0) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                int idh = hc.GetHouse(value).id;
                _fight.HouseName2 = value;
                _fight.IdHouseChallenger2 = idh;
                base.OnPropertyChanged("HouseName2");
                addInBDD();
            }
        }
        public int IdWinningHouse
        {
            get { return _fight.IdWinningHouse; }
            set
            {
                if (value == _fight.IdWinningHouse) return;
                _fight.IdWinningHouse = value;
                base.OnPropertyChanged("IdWinningHouse");
                addInBDD();
            }
        }
        public String HouseNameW
        {
            get { return _fight.HouseNameW; }
            set
            {
                if (value == _fight.HouseNameW) return;
                if (base.ExistHouseName(value) == 0) return;
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                int idh = hc.GetHouse(value).id;
                _fight.HouseNameW = value;
                _fight.IdWinningHouse = idh;
                base.OnPropertyChanged("HouseNameW");
                addInBDD();
            }
        }
        #endregion

    }
}


