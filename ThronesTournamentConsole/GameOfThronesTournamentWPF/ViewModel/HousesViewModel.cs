﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    class HousesViewModel : ViewModelBase
    {
        public event EventHandler<EventArgs> CloseAsk;

        protected void OnCloseAsk(EventArgs e)
            {
                this.CloseAsk(this, e);
            }

        private ObservableCollection<HouseViewModel> _houses;

        public ObservableCollection<HouseViewModel> Houses
        {
            get { return _houses; }
            private set
            {
                _houses = value;
                OnPropertyChanged("Houses");
            }
        }

        private HouseViewModel _selectedItem;
        public HouseViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }

        public HousesViewModel(IList<API.Models.HouseDTO> HousesModel)
        {
            _houses = new ObservableCollection<HouseViewModel>();
            foreach (API.Models.HouseDTO h in HousesModel)
            {
                _houses.Add(new HouseViewModel(h));
            }
        }

        #region "Commandes du formulaire"

        // Commande Add
        private RelayCommand _addCommand;
        public System.Windows.Input.ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(
                        () => this.Add(),
                        () => this.CanAdd()
                        );                    
                }
                return _addCommand;
            }
        }

        private bool CanAdd()
        {
            return true;
        }

        private void Add()
        {
            API.Models.HouseDTO h = new API.Models.HouseDTO("<New>", 0);
            this.SelectedItem = new HouseViewModel(h);
            Houses.Add(this.SelectedItem);
        }

        // Commande Remove
        private RelayCommand _removeCommand;
        public System.Windows.Input.ICommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(
                        () => this.Remove(),
                        () => this.CanRemove()
                        );
                }
                return _removeCommand;
            }
        }

        private bool CanRemove()
        {
            return (this.SelectedItem != null);
        }

        private void Remove()
        {
            if (this.SelectedItem != null)
            {
                API.Controllers.HouseController hc = new API.Controllers.HouseController();
                hc.DeleteHouse(this.SelectedItem.Id);
                Houses.Remove(this.SelectedItem);
            }
        }

        // Commande Close
        private RelayCommand _closeCommand;
        public System.Windows.Input.ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        () => this.Close(),
                        () => this.CanClose()
                        );
                }
                return _closeCommand;
            }
        }

        private bool CanClose()
        {
            return true;
        }

        private void Close()
        {
            OnCloseAsk(new EventArgs());
        }

        #endregion
    }

}
