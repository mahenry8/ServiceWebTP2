﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfThronesTournamentWPF.ViewModel
{
    class TerritoriesViewModel : ViewModelBase
    {
        public event EventHandler<EventArgs> CloseAsk;

        protected void OnCloseAsk(EventArgs e)
        {
            this.CloseAsk(this, e);
        }

        private ObservableCollection<TerritoryViewModel> _territories;

        public ObservableCollection<TerritoryViewModel> Territories
        {
            get { return _territories; }
            private set
            {
                _territories = value;
                OnPropertyChanged("Territories");
            }
        }
        private CtrlHouseViewModel _selectedHouse;
        public CtrlHouseViewModel SelectedHouse
        {
            get { return _selectedHouse; }
            set
            {
                _selectedHouse = value;
                OnPropertyChanged("SelectedHouse");
            }
        }

        private TerritoryViewModel _selectedItem;
        public TerritoryViewModel SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    API.Controllers.HouseController hc = new API.Controllers.HouseController();
                    API.Models.HouseDTO h = hc.GetHouse(value.IdOwner);
                    if (h == null)
                        _selectedHouse = new CtrlHouseViewModel(new API.Models.HouseDTO(-1, "Aucune", 0));
                    else
                        _selectedHouse = new CtrlHouseViewModel(h);
                    OnPropertyChanged("SelectedHouse");
                    OnPropertyChanged("SelectedItem");
                }

            }
        }

        public TerritoriesViewModel(IList<API.Models.TerritoryDTO> TerritoriesModel)
        {
            _territories = new ObservableCollection<TerritoryViewModel>();
            foreach (API.Models.TerritoryDTO t in TerritoriesModel)
            {
                _territories.Add(new TerritoryViewModel(t));
            }
        }

        #region "Commandes du formulaire"

        // Commande Add
        private RelayCommand _addCommand;
        public System.Windows.Input.ICommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(
                        () => this.Add(),
                        () => this.CanAdd()
                        );
                }
                return _addCommand;
            }
        }

        private bool CanAdd()
        {
            return true;
        }

        private void Add()
        {
            API.Models.TerritoryDTO c = new API.Models.TerritoryDTO(-1, EntitiesLayer.TerritoryType.DESERT,-1,"Aucune");
            this.SelectedItem = new TerritoryViewModel(c);
            this.SelectedItem.addInBDD();
            Territories.Add(this.SelectedItem);
            API.Controllers.TerritoryController ch = new API.Controllers.TerritoryController();
            SelectedItem.Id = ch.GetAllTerritories().Last().id;
        }

        // Commande Remove
        private RelayCommand _removeCommand;
        public System.Windows.Input.ICommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(
                        () => this.Remove(),
                        () => this.CanRemove()
                        );
                }
                return _removeCommand;
            }
        }

        private bool CanRemove()
        {
            return (this.SelectedItem != null);
        }

        private void Remove()
        {
            if (this.SelectedItem != null)
            {
                API.Controllers.TerritoryController hc = new API.Controllers.TerritoryController();
                hc.DeleteTerritory(this.SelectedItem.Id);
                Territories.Remove(this.SelectedItem);
            }
        }

        // Commande Close
        private RelayCommand _closeCommand;
        public System.Windows.Input.ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        () => this.Close(),
                        () => this.CanClose()
                        );
                }
                return _closeCommand;
            }
        }

        private bool CanClose()
        {
            return true;
        }

        private void Close()
        {
            OnCloseAsk(new EventArgs());
        }

        #endregion
    }

}
