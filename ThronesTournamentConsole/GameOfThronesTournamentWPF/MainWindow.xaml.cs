﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using API.Controllers;
using API.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace GameOfThronesTournamentWPF
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnOneHouse_Click(object sender, RoutedEventArgs e)
        {
            Forms.HouseViewer av = new GameOfThronesTournamentWPF.Forms.HouseViewer();
            av.ShowDialog();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnListHouses_Click(object sender, RoutedEventArgs e)
        {
            Forms.HousesViewer av = new GameOfThronesTournamentWPF.Forms.HousesViewer();
            av.ShowDialog();
        }

        private void btnListCharacters_Click(object sender, RoutedEventArgs e)
        {
            Forms.CharactersViewer av = new GameOfThronesTournamentWPF.Forms.CharactersViewer();
            av.ShowDialog();
        }
        private void btnListTerritories_Click(object sender, RoutedEventArgs e)
        {
            Forms.TerritoriesViewer av = new GameOfThronesTournamentWPF.Forms.TerritoriesViewer();
            av.ShowDialog();
        }
        private void btnListFights_Click(object sender, RoutedEventArgs e)
        {
            Forms.FightsViewer av = new GameOfThronesTournamentWPF.Forms.FightsViewer();
            av.ShowDialog();
        }

        private void btnExporter_Click(object sender, RoutedEventArgs e)
        {
            Forms.ExporterViewer av = new GameOfThronesTournamentWPF.Forms.ExporterViewer();
            av.ShowDialog();
        }
    }

}
