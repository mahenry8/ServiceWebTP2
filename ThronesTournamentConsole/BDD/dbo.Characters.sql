﻿CREATE TABLE [dbo].[Table]
(
	[IdCharacter] INT NOT NULL PRIMARY KEY, 
    [Bravoury] INT NULL, 
    [Crazyness] INT NULL, 
    [FirstName] VARCHAR(50) NOT NULL, 
    [LastName] VARCHAR(50) NOT NULL, 
    [Pv] INT NULL
)
