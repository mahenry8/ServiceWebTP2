﻿CREATE TABLE [dbo].[Territories]
(
	[IdTerritory] INT NOT NULL PRIMARY KEY, 
    [Type] VARCHAR(50) NULL, 
    [Owner] VARCHAR(50) NULL, 
    CONSTRAINT [Owner] FOREIGN KEY ([Owner]) REFERENCES [Houses]([Name])
)
