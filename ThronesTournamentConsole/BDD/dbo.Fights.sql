﻿CREATE TABLE [dbo].[Fights]
(
	[IdFight] INT NOT NULL PRIMARY KEY, 
    [HouseName1] VARCHAR(50) NULL, 
    [HouseName2] VARCHAR(50) NULL, 
    [WinningHouse] VARCHAR(50) NULL, 
    [WarName] VARCHAR(50) NULL, 
    CONSTRAINT [HouseName1] FOREIGN KEY ([HouseName1]) REFERENCES [Houses]([Name]), 
    CONSTRAINT [HouseName2] FOREIGN KEY ([HouseName2]) REFERENCES [Houses]([Name]), 
    CONSTRAINT [WinningHouse] FOREIGN KEY ([WinningHouse]) REFERENCES [Houses]([Name])
)
