﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace API.Controllers
{
    public class CharacterController : ApiController
    {
        [Route("api/GetAllCharacters")]
        public List<CharacterDTO> GetAllCharacters()
        {
            List<CharacterDTO> list = new List<CharacterDTO>();
            DalManager m = DalManager.Instance;

            foreach (var charac in m.ExistingCharacters())
            {
                CharacterDTO c = new CharacterDTO();
                c.id = charac.id;
                c.Bravoury = charac.Bravoury;
                c.Crazyness = charac.Crazyness;
                c.FirstName = charac.FirstName;
                c.LastName = charac.LastName;
                c.Pv = charac.Pv;
                c.IdHouse = charac.IdHouse;
                c.HouseName = charac.HouseName;
                c.CharacterType = charac.CharacterType;
                list.Add(c);
            }
            return list;
        }
        [Route("api/GetCharacter/{ln}/{fn}")]
        public CharacterDTO GetCharacter(int id)
        {
            DalManager m = DalManager.Instance;
            //EntitiesLayer.Character charac = m.GetCharacterByName(c0.LastName, c0.FirstName);
            EntitiesLayer.Character charac = m.GetCharacterById(id);
            if (charac == null)
                return null;
            CharacterDTO c = new CharacterDTO();            
            c.id = charac.id;
            c.Bravoury = charac.Bravoury;
            c.Crazyness = charac.Crazyness;
            c.FirstName = charac.FirstName;
            c.LastName = charac.LastName;
            c.Pv = charac.Pv;
            c.IdHouse = charac.IdHouse;
            c.HouseName = charac.HouseName;
            c.CharacterType = charac.CharacterType;
            return c;
        }

        [Route("api/AddCharacter/{b:int}/{c:int}/{fn}/{ln}/{p:int}/{hn}/{t}")]
        [HttpPost]
        public void AddCharacter(CharacterDTO c)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Character charac = new EntitiesLayer.Character(c.Bravoury, c.Crazyness, c.FirstName, c.LastName, c.Pv, c.IdHouse,c.HouseName, c.CharacterType);
            m.AddCharacterInBDD(charac);
        }

        [Route("api/AddCharacter/{b:int}/{c:int}/{fn}/{ln}/{p:int}/{hn}/{t}")]
        [HttpDelete]
        public void DeleteCharacter(int id)
        {
            DalManager m = DalManager.Instance;
            m.DeleteCharacterInBDD(id);
        }

        [Route("api/PutCharacter/{b:int}/{c:int}/{fn}/{ln}/{p:int}/{hn}/{t}")]
        [HttpPut]
        public void PutCharacter(CharacterDTO c)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Character charac = new EntitiesLayer.Character(c.id,c.Bravoury, c.Crazyness, c.FirstName, c.LastName, c.Pv, c.IdHouse,c.HouseName, c.CharacterType);
            m.UpdateCharacterInBDD(charac);
        }

        [Route("api/AddRelation/{fn1}/{ln1}/{fn2}/{ln2}/{r}")]
        [HttpPost]
        public void AddRelation(int id1, int id2, String r)
        {
            DalManager m = DalManager.Instance;
            /*EntitiesLayer.Character perso1 = m.GetCharacterByName(c1.LastName, c1.FirstName);
            EntitiesLayer.Character perso2 = m.GetCharacterByName(c2.LastName, c2.FirstName);*/
            m.AddRelationshipInBDD((EntitiesLayer.RelationshipEnum)Enum.Parse(typeof(EntitiesLayer.RelationshipEnum), r), id1, id2);
        }

        [Route("api/PutRelation/{fn1}/{ln1}/{fn2}/{ln2}/{r}")]
        [HttpPost]
        public void PutRelation(int idr, int id1, int id2, String r)
        {
            DalManager m = DalManager.Instance;
            /*EntitiesLayer.Character perso1 = m.GetCharacterByName(c1.LastName, c1.FirstName);
            EntitiesLayer.Character perso2 = m.GetCharacterByName(c2.LastName, c2.FirstName);*/
            m.UpdateRelationshipInBDD((EntitiesLayer.RelationshipEnum)Enum.Parse(typeof(EntitiesLayer.RelationshipEnum), r), id1, id2);
        }
        [Route("api/DeleteRelation/{fn1}/{ln1}/{fn2}/{ln2}/{r}")]
        [HttpDelete]
        public void DeleteRelation(int id1, int id2)
        {
            DalManager m = DalManager.Instance;
            /*EntitiesLayer.Character perso1 = m.GetCharacterByName(c1.LastName, c1.FirstName);
            EntitiesLayer.Character perso2 = m.GetCharacterByName(c2.LastName, c2.FirstName);*/
            m.DeleteRelationshipInBDD(id1, id2);
        }
    }

}
