﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.Models;
using BusinessLayer;

namespace API.Controllers
{
    public class GameController : ApiController
    {
        [Route("api/DoFight/{h1}/{h2}/{t:int}/{idWar:int}")]
        [HttpGet]
        public String DoFight(int id1, int id2, int t, int idWar)
        {
            //TODO Envoyer paramètres => CreerWar puis DoFight BIM resultat on affiche 
            DalManager m = DalManager.Instance;
            BusinessLayer.BusinessGame b = new BusinessGame(m);
            EntitiesLayer.House house1 = m.GetHouseById(id1);
            EntitiesLayer.House house2 = m.GetHouseById(id2);
            EntitiesLayer.Territory terr = m.GetTerritoryById(t);
            switch (b.Fight(house1, house2, terr, idWar))
            {
                case 0:
                    return "Le combat s'est effectué, les deux maisons sont toujours en vie";
                case 1:
                    return "La maison " + house2.Name + " a gagné";
                default:
                    return "La maison " + house1.Name + " a gagné";
            }
        }
    }
}
