﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class FightController : ApiController
    {
        [Route("api/GetAllFights")]
        public List<FightDTO> GetAllFights()
        {
            List<FightDTO> list = new List<FightDTO>();
            DalManager m = DalManager.Instance;

            foreach (var fig in m.ExistingFights())
            {
                FightDTO f = new FightDTO();
                f.id = fig.id;
                f.IdHouseChallenger1 = fig.IdHouseChallenger1;
                f.IdHouseChallenger2 = fig.IdHouseChallenger2;
                f.IdWinningHouse = fig.IdWinningHouse;
                list.Add(f);
            }
            return list;
        }

        [Route("api/GetFight/{id:int}")]
        public FightDTO GetFight(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Fight fig = m.GetFightById(id);
            if (fig == null)
                return null;
            FightDTO f = new FightDTO();
            f.id = fig.id;
            f.IdHouseChallenger1 = fig.IdHouseChallenger1;
            f.IdHouseChallenger2 = fig.IdHouseChallenger2;
            f.IdWinningHouse = fig.IdWinningHouse;
            return f;
        }

        [Route("api/AddFight/{hc1}/{hc2}/{idw:int}")]
        [HttpPost]
        public void AddFight(FightDTO f)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Fight fight = new EntitiesLayer.Fight(f.id,f.IdHouseChallenger1, f.IdHouseChallenger2, f.IdWinningHouse, f.IdWar);
            m.AddFightInBDD(fight);
        }

        [Route("api/DeleteFight/{id:int}")]
        [HttpDelete]
        public void DeleteFight(int id)
        {
            DalManager m = DalManager.Instance;
            m.DeleteFightInBDD(id);
        }

        [Route("api/PutFight/{hc1}/{hc2}/{idw:int}")]
        [HttpPut]
        public void PutFight(FightDTO f)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Fight fight = new EntitiesLayer.Fight(f.id,f.IdHouseChallenger1, f.IdHouseChallenger2, f.IdWinningHouse, f.IdWar);
            m.UpdateFightInBDD(fight);
        }
    }
}
