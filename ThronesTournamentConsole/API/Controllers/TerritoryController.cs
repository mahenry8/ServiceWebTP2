﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class TerritoryController : ApiController
    {
        [Route("api/GetAllTerritories")]
        public List<TerritoryDTO> GetAllTerritories()
        {
            List<TerritoryDTO> list = new List<TerritoryDTO>();
            DalManager m = DalManager.Instance;

            foreach (var terr in m.ExistingTerritories())
            {
                TerritoryDTO t = new TerritoryDTO();
                t.id = terr.id;
                t.Type = terr.Type;
                t.IdOwner = terr.IdOwner;
                t.HouseName = terr.HouseName;
                list.Add(t);
            }
            return list;
        }

        [Route("api/GetTerritory/{id:int}")]
        public TerritoryDTO GetTerritory(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Territory terr = m.GetTerritoryById(id);
            TerritoryDTO t = new TerritoryDTO();
            t.id = terr.id;
            t.Type = terr.Type;
            t.IdOwner = terr.IdOwner;
            t.HouseName = terr.HouseName;
            return t;
        }

        [Route("api/AddTerritory/{type}/{owner}/{id:int}")]
        [HttpPost]
        public void AddTerritory(TerritoryDTO t)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Territory terr = new EntitiesLayer.Territory(t.Type, t.IdOwner);
            m.AddTerritoryInBDD(terr);
        }

        [Route("api/DeleteTerritory/{id:int}")]
        [HttpDelete]
        public void DeleteTerritory(int id)
        {
            DalManager m = DalManager.Instance;
            m.DeleteTerritoryInBDD(id);
        }

        [Route("api/PutTerritory/{type}/{owner}/{id:int}")]
        [HttpPut]
        public void PutTerritory(TerritoryDTO t)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.Territory terr = new EntitiesLayer.Territory(t.id,t.Type, t.IdOwner);
            m.UpdateTerritoryInBDD(terr);
        }
    }
}
