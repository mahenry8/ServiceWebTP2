﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace API.Controllers
{
    public class HouseController : ApiController
    {
        [Route("api/GetAllHouses")]
        public List<HouseDTO> GetAllHouses()
        {
            List<HouseDTO> list = new List<HouseDTO>();
            DalManager m = DalManager.Instance;

            foreach (var house in m.ExistingHouses())
            {
                HouseDTO c = new HouseDTO();
                c.id = house.Id;
                c.Name = house.Name;
                c.NumberOfUnities = house.NumberOfUnities;
                list.Add(c);
            }
            return list;
        }

        [Route("api/GetHouse/{n}")]
        public HouseDTO GetHouse(int id)
        {
            DalManager m = DalManager.Instance;
            //EntitiesLayer.House house = m.GetHouseByName(n);
            EntitiesLayer.House house = m.GetHouseById(id);
            if (house == null)
                return null;
            HouseDTO c = new HouseDTO();
            c.id = house.Id;
            c.Name = house.Name;
            c.NumberOfUnities = house.NumberOfUnities;
            return c;
        }
        [Route("api/GetHouse/{n}")]
        public HouseDTO GetHouse(String n)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.House house = m.GetHouseByName(n);
            //EntitiesLayer.House house = m.GetHouseById(id);
            HouseDTO c = new HouseDTO();
            c.id = house.Id;
            c.Name = house.Name;
            c.NumberOfUnities = house.NumberOfUnities;
            return c;
        }

        [Route("api/AddHouse/{name}/{NoU:int}")]
        [HttpPost]
        public void AddHouse(HouseDTO h)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.House house = new EntitiesLayer.House(h.Name, h.NumberOfUnities);
            m.AddHouseInBDD(house);
        }

        [Route("api/DeleteHouse/{name}")]
        [HttpDelete]
        public void DeleteHouse(int id)
        {
            DalManager m = DalManager.Instance;
            //EntitiesLayer.House house = new EntitiesLayer.House(h.id, h.Name, h.NumberOfUnities);
            m.DeleteHouseInBDD(id);
        }

        [Route("api/PutHouse/{name}/{NoU:int}")]
        [HttpPut]
        public void PutHouse(HouseDTO h)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.House house = new EntitiesLayer.House(h.id, h.Name, h.NumberOfUnities);
            m.UpdateHouseInBDD(house);
        }
    }
}
