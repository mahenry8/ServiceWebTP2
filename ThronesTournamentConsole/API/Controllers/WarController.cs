﻿using API.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace API.Controllers
{
    public class WarController : ApiController
    {
        [Route("api/GetAllWars")]
        public List<WarDTO> GetAllWars()
        {
            List<WarDTO> list = new List<WarDTO>();
            DalManager m = DalManager.Instance;

            foreach (var w in m.ExistingWars())
            {
                WarDTO war = new WarDTO();
                war.name = w.name;
                war.id = w.id;
                list.Add(war);
            }
            return list;
        }

        [Route("api/GetWars")]
        public WarDTO GetWar(int id)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.War w = m.GetWarById(id);
            WarDTO war = new WarDTO();
            war.name = w.name;
            war.id = w.id;
            return war;
        }

        [Route("api/AddWar/{id:int}/{name}")]
        [HttpPost]
        public void AddWar(WarDTO w)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.War war = new EntitiesLayer.War(w.name);
            m.AddWarInBDD(war);
        }

        [Route("api/DeleteWar/{id:int}")]
        [HttpDelete]
        public void DeleteWar(int id)
        {
            DalManager m = DalManager.Instance;
            m.DeleteWarInBDD(id);
        }

        [Route("api/PutWar/{id:int}/{name}")]
        [HttpPut]
        public void PutWar(WarDTO w)
        {
            DalManager m = DalManager.Instance;
            EntitiesLayer.War war = new EntitiesLayer.War(w.name);
            m.UpdateWarInBDD(war);
        }
    }
}
