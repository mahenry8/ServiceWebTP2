﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class FightDTO
    {
        public int IdHouseChallenger1 { get; set; }
        public int IdHouseChallenger2 { get; set; }
        public int IdWinningHouse { get; set; }
        public int IdWar { get; set; }
        public int id { get; set; }
        public String HouseName1 { get; set; }
        public String HouseName2 { get; set; }
        public String HouseNameW { get; set; }

        public FightDTO() { }

        public void AddWinningHouse(int wh)
        {
            IdWinningHouse = wh;
        }
        public FightDTO(int ih1, int ih2,int ihw, int iw)
        {
            IdHouseChallenger1 = ih1;
            IdHouseChallenger2 = ih2;
            IdWinningHouse = ihw;
            IdWar = iw;
        }
        public FightDTO(int i,int ih1, int ih2, int ihw, int iw)
        {
            id = i;
            IdHouseChallenger1 = ih1;
            IdHouseChallenger2 = ih2;
            IdWinningHouse = ihw;
            IdWar = iw;
        }
    }
}