﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class WarDTO
    {
        public String name { get; set; }
        public int id { get; set; }

        public WarDTO() { }

        public WarDTO(String n)
        {
            name = n;
        }
        public WarDTO(int i, String n)
        {
            id = i;
            name = n;
        }
    }
}