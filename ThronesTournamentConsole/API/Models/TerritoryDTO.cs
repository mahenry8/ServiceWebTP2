﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class TerritoryDTO
    {
        public EntitiesLayer.TerritoryType Type { get; set; }
        public int IdOwner { get; set; }
        public int id { get; set; }
        public String HouseName { get; set; }

        public TerritoryDTO() { }
        public TerritoryDTO(EntitiesLayer.TerritoryType t, int io, String hn)
        {
            Type = t;
            IdOwner = io;
            HouseName = hn;
        }
        public TerritoryDTO(int i,EntitiesLayer.TerritoryType t, int io, String hn)
        {
            id = i;
            Type = t;
            IdOwner = io;
            HouseName = hn;
        }
    }
}