﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    public class HouseDTO
    {
        public int id { get; set; }
        public String Name { get; set; }
        public int NumberOfUnities { get; set; }

        public HouseDTO() { }
        public HouseDTO(int i, String n, int nbo)
        {
            id = i;
            Name = n;
            NumberOfUnities = nbo;
        }
        public HouseDTO(String n, int nbo)
        {
            Name = n;
            NumberOfUnities = nbo;
        }

    }
}