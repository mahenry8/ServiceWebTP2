﻿using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API.Models
{
    [SerializableAttribute]
    public class CharacterDTO
    {
        public int id { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public int Pv { get; set; }
        public int IdHouse { get; set; }
        public String HouseName { get; set; }
        public CharacterTypeEnum CharacterType { get; set; }

        public CharacterDTO() { }
        public CharacterDTO(int b, int c, String fn, String ln, int p, int idh, string hn, CharacterTypeEnum t) {
            Bravoury = b;
            Crazyness = c;
            FirstName = fn;
            LastName = ln;
            Pv = p;
            IdHouse = idh;
            HouseName = hn;
            CharacterType = t;
        }
        public CharacterDTO(int i, int b, int c, String fn, String ln, int p, int idh, string hn,CharacterTypeEnum t)
        {
            id = i;
            Bravoury = b;
            Crazyness = c;
            FirstName = fn;
            LastName = ln;
            Pv = p;
            IdHouse = idh;
            HouseName = hn;
            CharacterType = t;
        }


    }
}