﻿using DataAccessLayer;
using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BusinessGame
    {
        public DalManager dal;

        public BusinessGame(DalManager d)
        {
            dal = d;
        }

        public int Fight(House h1, House h2, Territory t, int idwar)
        {
            dal = DalManager.Instance;

            foreach (Character c1 in h1.Housers)
            {
                foreach (Character c2 in h2.Housers)
                {
                    if (c1.Crazyness > c2.Crazyness)
                        Attaque(c1, c2, t);
                    else
                        Attaque(c2, c1, t);
                }
            }
            Random random = new Random();
            h1.NumberOfUnities -= random.Next(500);
            h2.NumberOfUnities -= random.Next(500);

            dal.UpdateHouseInBDD(h1);
            dal.UpdateHouseInBDD(h2);

            Fight f = new EntitiesLayer.Fight(h1.Id, h2.Id, idwar, -1);
            if (h1.Housers.Count == 0 || h1.NumberOfUnities <= 0)
            {
                f.AddWinningHouse(h1.Id);
                dal.AddFightInBDD(f);
                return 1;
            }
            else if (h2.Housers.Count == 0 || h2.NumberOfUnities <= 0)
            {
                f.AddWinningHouse(h2.Id);
                dal.AddFightInBDD(f);
                return 2;
            }
            else
            {
                dal.AddFightInBDD(f);
                return 0;
            }
        }
        public void Attaque(Character c1, Character c2, Territory t)
        {
            double coefRelation;
            double coefTerritory;
            /*RelationshipEnum relation = c1.Relationships.Find(x => x.Item2 == c2).Item1;
            switch (relation)
            {
                case RelationshipEnum.FRIENDSHIP:
                    coefRelation = 0.5;
                    break;
                case RelationshipEnum.HATRED:
                    coefRelation = 4;
                    break;
                case RelationshipEnum.LOVE:
                    coefRelation = 0.25;
                    break;
                case RelationshipEnum.RIVALRY:
                    coefRelation = 2;
                    break;
                default:
                    coefRelation = 1;
                    break;
            }*/
            coefRelation = 1;
            switch (t.Type)
            {
                case EntitiesLayer.TerritoryType.DESERT:
                    coefTerritory = 0.75;
                    break;
                case EntitiesLayer.TerritoryType.LAND:
                    coefTerritory = 0.25;
                    break;
                case EntitiesLayer.TerritoryType.MOUNTAIN:
                    coefTerritory = 0.5;
                    break;
                case EntitiesLayer.TerritoryType.SEA:
                    coefTerritory = 0.85;
                    break;
                default:
                    coefTerritory = 1;
                    break;
            }

            if (t.IdOwner == c1.IdHouse)
            {
                c2.Pv -= (int)((10 + c1.Crazyness - c2.Bravoury) * coefRelation * (2 - coefTerritory));
                if (c2.Pv > 0)
                    c1.Pv -= (int)((10 + c2.Crazyness - c1.Bravoury) * coefRelation * coefTerritory);
            }
            else
            {
                c1.Pv -= (int)((10 + c2.Crazyness - c1.Bravoury) * coefRelation * (2 - coefTerritory));
                if (c1.Pv > 0)
                    c2.Pv -= (int)((10 + c1.Crazyness - c2.Bravoury) * coefRelation * coefTerritory);
            }
            if (c1.Pv > 0)
                dal.UpdateCharacterInBDD(c1);
            else
                dal.DeleteCharacterInBDD(c1.id);

            if (c2.Pv > 0)
                dal.UpdateCharacterInBDD(c2);
            else
                dal.DeleteCharacterInBDD(c2.id);

        }

        public String getHouses()
        {
            String result = "";
            List<EntitiesLayer.House> Houses = dal.ExistingHouses();
            foreach (House h in Houses)
            {
                result += h.Name + " ";
            }
            return result;
        }

        public String getHousesSup200U()
        {
            String result = "";
            List<EntitiesLayer.House> Houses = dal.ExistingHouses();
            var selectedHouses = from h in Houses where h.NumberOfUnities > 200 select h.Name;
            foreach (String n in selectedHouses)
            {
                result += n + " ";
            }
            return result;
        }

        public String getStrongCharacters()
        {
            String result = "";
            List<EntitiesLayer.Character> Characters = dal.ExistingCharacters();
            var selectedCharacters = from c in Characters where c.Bravoury > 3 && c.Pv > 50 select c.FirstName + " " + c.LastName;
            foreach (String n in selectedCharacters)
            {
                result += n + " ";
            }
            return result;
        }

        public String getTerritories()
        {
            String result = "";
            List<EntitiesLayer.Territory> Territories = dal.ExistingTerritories();
            foreach (Territory t in Territories)
            {
                result += t.Type + " ";
            }
            return result;
        }
    }
}
