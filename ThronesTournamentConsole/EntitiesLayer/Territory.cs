﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    [Serializable]
    public class Territory : EntityObject
    {
        public TerritoryType Type { get; set; }
        public int IdOwner { get; set; }
        public int id { get; set; }
        public String HouseName { get; set; }

        public Territory(int i, TerritoryType t, int o)
        {
            Type = t;
            IdOwner = o;
            id = i;
        }

        public Territory(TerritoryType t, int o)
        {
            Type = t;
            IdOwner = o;
        }
        public Territory(int i, TerritoryType t, int o, String hn)
        {
            Type = t;
            IdOwner = o;
            id = i;
            HouseName = hn;
        }

        public Territory(TerritoryType t, int o, String hn)
        {
            Type = t;
            IdOwner = o;
            HouseName = hn;
        }
    }
}
