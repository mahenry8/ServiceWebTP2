﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    [Serializable]
    public class Fight : EntityObject
    {
        public int IdHouseChallenger1 { get; set; }
        public int IdHouseChallenger2 { get; set; }
        public int IdWinningHouse { get; set; }
        public int idWar { get; set; }
        public int id { get; set; }

        public Fight(int i, int id1, int id2, int iw)
        {
            IdHouseChallenger1 = id1;
            IdHouseChallenger2 = id2;
            idWar = iw;
            id = i;
        }
        public Fight(int i, int id1, int id2,int iwh, int iw)
        {
            IdHouseChallenger1 = id1;
            IdHouseChallenger2 = id2;
            IdWinningHouse = iwh;
            idWar = iw;
            id = i;
        }
        public void AddWinningHouse(int idw)
        {
            IdWinningHouse = idw;
        }


    }
}
