﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    [Serializable]
    public class War : EntityObject
    {
        public List<Fight> combats { get; set; }
        public String name { get; set; }
        public int id { get; set; }

        public War(int i, String n)
        {
            name = n;
            combats = new List<Fight>();
            id = i;
        }
        public War(String n)
        {
            name = n;
            combats = new List<Fight>();
        }

        public void AddFight(Fight f)
        {
            combats.Add(f);
        }
        public void RemoveFight(Fight f)
        {
            combats.Remove(f);
        }
    }
}
