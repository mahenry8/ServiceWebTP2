﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    [Serializable]
    public class Character : EntityObject
    {
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public int Pv { get; set; }
        public int IdHouse { get; set; }
        public String HouseName { get; set; }
        public List<Tuple<RelationshipEnum, int>> Relationships { get; set; }
        public CharacterTypeEnum CharacterType { get; set; }
        public int id { get; set; }

        public void AddRelatives(RelationshipEnum relation, Character c)
        {
            Tuple<RelationshipEnum, int> tmp = new Tuple<RelationshipEnum, int>(relation, c.id);
            Relationships.Add(tmp);
        }
        public Character(int i, int brav, int craz, String firstn, String lastn, int p, int idh, CharacterTypeEnum t)
        {
            Bravoury = brav;
            Crazyness = craz;
            FirstName = firstn;
            LastName = lastn;
            Pv = p;
            IdHouse = idh;
            Relationships = new List<Tuple<RelationshipEnum, int>>();
            CharacterType = t;
            id = i;
        }
        public Character(int brav, int craz, String firstn, String lastn, int p, int idh, CharacterTypeEnum t)
        {
            Bravoury = brav;
            Crazyness = craz;
            FirstName = firstn;
            LastName = lastn;
            Pv = p;
            IdHouse = idh;
            Relationships = new List<Tuple<RelationshipEnum, int>>();
            CharacterType = t;
        }

        public Character(int i, int brav, int craz, String firstn, String lastn, int p, int idh,String hn, CharacterTypeEnum t)
        {
            Bravoury = brav;
            Crazyness = craz;
            FirstName = firstn;
            LastName = lastn;
            Pv = p;
            IdHouse = idh;
            HouseName = hn;
            Relationships = new List<Tuple<RelationshipEnum, int>>();
            CharacterType = t;
            id = i;
        }
        public Character(int brav, int craz, String firstn, String lastn, int p, int idh, String hn, CharacterTypeEnum t)
        {
            Bravoury = brav;
            Crazyness = craz;
            FirstName = firstn;
            LastName = lastn;
            Pv = p;
            IdHouse = idh;
            HouseName = hn;
            Relationships = new List<Tuple<RelationshipEnum, int>>();
            CharacterType = t;
        }
        override
        public String ToString()
        {
            String result = FirstName + " " + LastName + " a " + Bravoury + " de bravoure, " + Crazyness + " de folie " + Pv + " de Pv, ";
            foreach (Tuple<RelationshipEnum, int> s in Relationships)
                result += " et a la relation : " + s.Item1 + " avec " + s.Item2 + " " + s.Item2;
            return result;
        }
    }
}
