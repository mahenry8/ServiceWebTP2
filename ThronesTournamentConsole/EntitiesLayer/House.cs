﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    [Serializable]
    public class House : EntityObject
    {
        public List<Character> Housers { get; set; }
        public String Name { get; set; }
        public int NumberOfUnities { get; set; }
        public int Id { get; set; }

        public void AddHousers(Character c)
        {
            Housers.Add(c);
        }
        public void RemoveHousers(Character c)
        {
            Housers.Remove(c);
        }

        public House(String nom)
        {
            Name = nom;
            Housers = new List<Character>();
            NumberOfUnities = 0;
        }
        public House(int i, String nom, int n)
        {
            Id = i;
            Name = nom;
            Housers = new List<Character>();
            NumberOfUnities = n;
        }
        public House(String nom, int n)
        {
            Name = nom;
            Housers = new List<Character>();
            NumberOfUnities = n;
        }

        public void deleteUnities(int nb)
        {
            NumberOfUnities -= nb;
        }


    }
}
