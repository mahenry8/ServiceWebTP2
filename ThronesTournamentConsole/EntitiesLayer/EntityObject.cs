﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public enum RelationshipEnum { FRIENDSHIP, LOVE, HATRED, RIVALRY };
    public enum CharacterTypeEnum { WARRIOR, WITCH, TACTICIAN, LEADER, LOSER };
    public enum TerritoryType { SEA, MOUNTAIN, LAND, DESERT };

    public abstract class EntityObject
    {
        private int id;
        private static int next_id = 0;

        public EntityObject()
        {
            id = next_id;
            next_id++;
        }

        public int getId()
        {
            return id;
        }
    }
}
